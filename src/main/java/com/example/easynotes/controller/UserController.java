package com.example.easynotes.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Cihan Yılmaz on 3.04.2019.
 */
@RestController
public class UserController {

    @RequestMapping("/deneme")
    public String home() {
        return "Spring boot is working!";
    }

}
